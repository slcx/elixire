sanic==0.7.0
sanic-cors==0.9.3

asyncpg==0.15.0
aioredis==1.1.0
bcrypt==3.1.4
itsdangerous==0.24

cerberus==1.1

# The worst idea ever is to add aiohttp to a project.
# and I did it.
# fuck aiohttp.
aiohttp==3.0.3

# May god have mercy on my soul
Pillow==5.0.0

# testing stuff
pytest==3.5.0
pyflakes==1.6.0
pytest-sanic==0.1.9
